const express = require('express');
const session = require('express-session');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(session
    ({
        secret: 'KkXEIB@E;AVQg:+8!?TmCc~t#z?Uplyb|_;8]{&$5:s?44%#}H$(8,bA2S%oYB_',
        resave: false,
        saveUninitialized: true,
    }));


app.post('/task', (req, res) => {
    req.session.tasks.push({
        title: req.body.title,
        done: false
    });
    res.redirect('/');
});
app.get('/task/:id/done', (req, res) => {
    if(req.session.tasks[req.params.id]){
        req.session.tasks[req.params.id].done = true;
    }
   
    res.redirect('/');
});
app.get('/task/:id/delete', (req, res) => {
    if(req.session.tasks[req.params.id]){
        req.session.tasks.splice(req.params.id, 1);
    }
     res.redirect('/');
 });
app.get('/', (req, res) => {
    if(!req.session.tasks){
        req.session.tasks = [];
    }
    res.render('index', { title: 'TodoList', tasks: req.session.tasks });
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});